# Tuiles

OpenStreetMap, serveur de tuiles conteneurisé.
Serveur de carte vectorielle et matricielle, compatible avec les clients MapboxGL, Android, iOS, Leaflet, OpenLayers, GIS via WMTS, etc.

Ce projet permet de monter une infrastructure de serveur de tuiles.
Ce docker ouvre un port 80 et un port 443.

***Tuiles est une implémentation du projet opensource [tileserver](https://hub.docker.com/r/klokantech/tileserver-gl/).***

Ce document traite de :
- L'installation du projet git
- Le montage d'une architecture serveur de tuiles
- des exemples de parametrage
- la fabrication d'une carte au format .mbtiles


# Installation

Une fois le projet cloné, le lancement de l'application se réalise simplement à la racine du projet par :
```
$> git clone ssh://git@git.beta.pole-emploi.fr:23/open-source/tuiles.git
$> cd tuiles
tuiles$> sudo docker-compose up -d --build
```
Le projet peut se paramétrer au niveau des ports, token... par le biais du fichier `.env` lu par docker-compose.

***Pour l'installation de docker, suivre les prérequis en annexes.***


# Montage d'une architecture serveur de tuiles avec 2 serveurs + 1 load balancer

On considérera que :
- le domaine maps.domain.tld pointe le load balancer
- le domaine mapsft1.domain.tld pointe le premier serveur de tuiles
- le domaine mapsft2.domain.tld pointe le deuxième serveur de tuiles
- docker et docker-compose sont installés (voir annexes plus bas)
- nous disposons d'une carte de la planète au format `.mbtiles`: `planet.mbtiles`

Et on considérera que le projet git est cloné :
```
$> git clone ssh://git@git.beta.pole-emploi.fr:23/open-source/tuiles.git
$> cd tuiles
```

## Serveur de répartition de charge: load balancer à l'adresse `maps.domain.tld`

Création d'abord d'un .env :
```
SSL=letsencrypt
USE_CACHE=true
MAPS_TOKEN=0bf68e49728c59e806807d4bf9082b53f94a6c096ae33434ad4b1e2686b97efe # Valeur pour exemple à modifier pour un autre hash aléatoire en production
BALANCER_IPLIST=mapsft1.domain.tld mapsft2.domain.tld
```

Puis copie d'une carte de la planete Terre dans `data/` et démarrage du conteneur : (voir plus bas comment fabriquer une carte)
```
tuiles$> cp planet.mbtiles data/
tuiles$> docker-compose up -d --build
```

## Premier serveur de tuiles 

Création d'abord d'un .env : (utilisation du token du load balancer qui indique de télécharger la map sur le load balancer)
```
URL_DOWNLOADMAP=https://maps.domain.tld/0bf68e49728c59e806807d4bf9082b53f94a6c096ae33434ad4b1e2686b97efe/planet.mbtiles
```

Puis démarrage du conteneur :
```
tuiles$> docker-compose up -d --build
```

## Deuxième serveur de tuiles (identique au premier)

Création d'abord d'un .env : (utilisation du token du load balancer)
```
URL_DOWNLOADMAP=https://maps.domain.tld/0bf68e49728c59e806807d4bf9082b53f94a6c096ae33434ad4b1e2686b97efe/planet.mbtiles
```

Puis démarrage du conteneur :
```
tuiles$> docker-compose up -d --build
```

# Paramétrage

L'application peut se paramétrer par le biais d'un fichier `.env` à créer à la racine du projet :

Paramètre              | Options                                                           | Defaut             |
-----------------------|:------------------------------------------------------------------|:-------------------|
DOMAIN                 | nom de domaine web                                                | maps.domaine.tld   |
HTTP_PORT              | port http                                                         | 80                 |
HTTPS_PORT             | port https                                                        | 443                |
BALANCER_IPLIST        | liste des IP de serveurs de tuiles (ce qui active le repartiteur) | 127.0.0.1          |
SSL                    | true\|false\|letsencrypt                                          | false              |
SSL_LETSENCRYPTMAIL    | mail de référence letsencrypt                                     | root@$DOMAIN       |
MAPS_TOKEN             | token permettant à d'autres serveurs de télécharger la map        | (sha256 aléatoire) |
USE_CACHE              | true (=500m)\|false\|valeur (ex: 1000m): active un cache nginx    | false              |
USE_MAP                | nom du fichiier de la map (.mbtiles) dans /data                   | (vide)             |
URL_DOWNLOADMAP        | URL de la map a télécharger et utiliser                           | (vide)             |

***Note: USE_MAP n'est utile que si le répertoire `data/` contient plusieurs cartes. Il indique laquelle utiliser***


## Différentes configurations et parametrage

***Exemple de fichier `.env` en mode répartiteur de charge :***
```
MAPS_TOKEN=0bf68e49728c59e806807d4bf9082b53f94a6c096ae33434ad4b1e2686b97efe
BALANCER_IPLIST=mapsft1.domain.tld:81 mapsft2.domain.tld mapsft3.domain.tld # Répartition de la charge sur 3 autres serveurs
```
***Dans cet exemple, le conteneur sera lancé en mode repartiteur de charge vers 3 autres serveurs, dont un ouvert sur port 81***


***Autre exemple de fichier `.env` en mode répartiteur de charge, avec ssl :***
```
SSL=letsencrypt
HTTP_PORT=80
HTTPS_PORT=443
DOMAIN=maps.domain.tld
MAPS_TOKEN=0bf68e49728c59e806807d4bf9082b53f94a6c096ae33434ad4b1e2686b97efe
BALANCER_IPLIST=mapsft1.domain.tld:81 mapsft2.domain.tld mapsft3.domain.tld
USE_CACHE=1000m # Spécifie un cache de requete de 1 go
```
***Dans cet exemple, le répartiteur de charge va chercher un certificat ssl letsencrypt.***

***Note: DOMAIN n'est utile qu'avec letsencrypt, pour négocier le certificat de ce domaine.***


Le répartiteur de charge peut se servir lui-même en ajoutant l'ip 127.0.0.1, auquel cas il devient lui aussi serveur de tuiles.

***Exemple simple suivant sans ssl :***
```
DOMAIN=maps.domain.tld
MAPS_TOKEN=0bf68e49728c59e806807d4bf9082b53f94a6c096ae33434ad4b1e2686b97efe
BALANCER_IPLIST=127.0.0.1 mapsft1.domain.tld mapsft2.domain.tld mapsft3.domain.tld
```
***Ici MAPS_TOKEN permet aux autres serveurs de tuiles de télécharger une map sur ce serveur, s'ils le veulent.***


## Configuration en serveur de tuiles

Les serveurs de tuiles servis par le répartiteur de charge peuvent être montés avec le même projet.
L'absence d'un fichier `.env` montera un simple serveur de tuiles. Toutefois il est conseillé de lui fournir quelques paramètres.


Exemple de fichier `.env` pour un noeud serveur de tuiles alimenté par le répartiteur de charge :
```
URL_DOWNLOADMAP=https://maps.domain.tld/0bf68e49728c59e806807d4bf9082b53f94a6c096ae33434ad4b1e2686b97efe/planet.mbtiles
```
***Dans cet exemple, le conteneur va tenter de télécharger une map `planet.mbtiles` sur le serveur de répartition avant de lancer le serveur de tuiles***


## Utilisation d'un certificat auto signé

S'assurer que le volume partagé `./tuiles/etc/nginx/ssl` soit vide et modifier le fichier `.env` avec le paramètre :
```
SSL=true
```

***Le conteneur va créer un jeu de clef dans `./tuiles/etc/nginx/ssl` : `fullchain.pem` et `privkey.pem`***


## Utilisation d'un jeu de clef certifié autre que letsencrypt

Positionner le jeu de clefs dans `./tuiles/etc/nginx/ssl` en les nommant de façon normée : `fullchain.pem` et `privkey.pem`.

Puis modifier le fichier `.env` avec le paramètre :
```
SSL=true
```

***Notons que par rapport au précédent cas, le conteneur detectera la présence des clefs et ne cherchera pas à creer un jeu auto-signé***



# Informations sur le fonctionnement

## Le serveur de dépot de carte

Un serveur de tuiles peut donc télécharger une carte au format `.mbtiles` via un serveur commun grâce à l'emploi de `MAPS_TOKEN`. Ce paramètre doit rester secret et utiliser des caractères alphanumériques uniquement.
Si possible utiliser pour `MAPS_TOKEN` un hash sha256.


## L'utilisation du serveur de dépot de carte par les serveurs de tuiles

Le paramètre `URL_DOWNLOADMAP` peut être utilisé pour spécifier à un noeud de se synchnoniser avec une autre carte. (téléchargement)
La carte téléchargée par chaque noeud est d'abord comparée en taille avec celle présente sur le disque. Si la carte est absente localement, ou différente en taille, un nouveau téléchargement se lance avant de rendre disponible le service de tuiles.

Les cartes téléchargées sont stockées dans le répertoire `data/`. Pour réinitialiser les cartes, il faut effacer le contenu de ce répertoire.


## Persistence des données via les volumes

Pour la persistence des données il est souhaitable de partager les répertoires suivants avec l'host :
- /data
- /etc/nginx/conf.d/.templates
- /etc/nginx/ssl
- /etc/letsencrypt
- /var/cache/munin/www
- /var/cache/nginx
- /var/log/nginx
- /var/lib/munin


## Les paramètrages à éviter 

- Un serveur de tuiles pouvant être serveur de dépôt, ne pas configurer le paramètre `URL_DOWNLOADMAP` sur lui-même: il risquerait d'écraser sa propre carte.




# Fabrication d'une carte au format `.mbtiles`

Il faut utiliser le projet open source [openmaptiles](https://github.com/openmaptiles/openmaptiles)
```
$> git clone https://github.com/openmaptiles/openmaptiles
$> cd openmaptiles
openmaptiles$> ./quickstart.sh france # Calcule la carte de france sur la base des données d'Open Street Map
```

Un fichier `.env` peut être paramétré pour spécifier des profondeurs de zoom ainsi que d'autres infos.
***La carte générée est accessible dans le répertoire `./data/` de ce projet, au format `.mbtiles`. Le fichier peut être copié sur le serveur servant de dépôt (voir plus haut).***


# Annexes

## Prérequis : installer docker et docker-compose

### Docker pour Ubuntu & Debian

```
$> sudo apt-get -y install apt-transport-https ca-certificates curl software-properties-common
$> sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
$> sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/$(lsb_release -is | tr '[:upper:]' '[:lower:]') $(lsb_release -cs) stable"
$> sudo apt-get update
$> sudo apt-get install -y docker-ce
$> sudo usermod -aG docker $USER
```

### Docker-compose (nécéssite Python)

```
$> sudo curl -L https://github.com/docker/compose/releases/download/1.25.0-rc2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
$> sudo chmod +x /usr/local/bin/docker-compose`
```
